// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "GladiatorPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class REGLADIATE_API AGladiatorPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = "State")
	void BP_ChangeState_Spectator();

	UFUNCTION(BlueprintCallable, Category = "State")
	void BP_ChangeState_Player();
	
};
