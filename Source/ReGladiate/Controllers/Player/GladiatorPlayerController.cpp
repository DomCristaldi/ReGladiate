// Fill out your copyright notice in the Description page of Project Settings.

#include "ReGladiate.h"
#include "GladiatorPlayerController.h"
#include "GameFramework/PlayerState.h"


void AGladiatorPlayerController::BP_ChangeState_Spectator()
{
	ChangeState(NAME_Spectating);
	if (Role == ROLE_Authority
		&& PlayerState != NULL)
	{
		PlayerState->bIsSpectator = true;
	}
}


void AGladiatorPlayerController::BP_ChangeState_Player()
{
	ChangeState(NAME_Playing);
	if (Role == ROLE_Authority
		&& PlayerState != NULL)
	{
		PlayerState->bIsSpectator = false;
	}
}
