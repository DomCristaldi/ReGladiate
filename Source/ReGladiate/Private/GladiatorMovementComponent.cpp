// Fill out your copyright notice in the Description page of Project Settings.


#include "ReGladiate.h"
#include "GladiatorMovementComponent.h"

#include "EngineGlobals.h"
#include "Engine/Engine.h"
#include "UnrealNetwork.h"

UGladiatorMovementComponent::UGladiatorMovementComponent(const class FObjectInitializer & ObjectInitializer)
	: Super(ObjectInitializer)
{

	//this->OnDestroyed.AddDynamic(this, )
}

void UGladiatorMovementComponent::InitializeComponent()
{
	Super::InitializeComponent();

}

void UGladiatorMovementComponent::DestroyComponent(bool bPromoteChildren)
{
	CleanupDodgeWhenDestroyed();

	Super::DestroyComponent(bPromoteChildren);
}

void UGladiatorMovementComponent::BeginPlay()
{
	//SavedMaxWalkSpeed = MaxWalkSpeed;
	//SavedBrakigDecelerationWalking = BrakingDecelerationWalking;

	if (currentGladiatorMoveMode != EGladiatorMovementMode::Walking
		|| MovementMode == EMovementMode::MOVE_Custom) 
	{
		SetMovementMode(EMovementMode::MOVE_Custom,
						(uint8)currentGladiatorMoveMode);
	}

}

void UGladiatorMovementComponent::TickComponent(float DeltaTime,
												enum ELevelTick TickType,
												FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime,
						 TickType,
						 ThisTickFunction);


}

void UGladiatorMovementComponent::PhysCustom(float deltaTime, int32 Iterations)
{
	switch (CustomMovementMode)
	{
		case (uint8)EGladiatorMovementMode::Boosting:
			PhysBoostMovement(deltaTime, Iterations);
			break;
		default:
			break;
	}
}

void UGladiatorMovementComponent::PhysBoostMovement(float deltaTime, int32 Iterations)
{

	if (deltaTime < MIN_TICK_TIME) { return; }

	if (!CharacterOwner							//if we don't have an Owner
		|| (!CharacterOwner->Controller			// or we do, but we're not configured correctly,
			&& !bRunPhysicsWithNoController		//  then do nothing
			&& !HasAnimRootMotion()
			&& !CurrentRootMotion.HasOverrideVelocity()
			&& (CharacterOwner->Role != ROLE_SimulatedProxy)))
	{
		Acceleration = FVector::ZeroVector;
		Velocity = FVector::ZeroVector;
		return;
	}

	if (!UpdatedComponent->IsQueryCollisionEnabled())
	{
		SetMovementMode(MOVE_Walking);
		return;
	}

	checkCode(ensureMsgf(!Velocity.ContainsNaN(),
						 TEXT("PhysBoostMovement: Velocity contains NaN before Iteration (%s)\n%s"),
							  *GetPathNameSafe(this),
							  *Velocity.ToString()));


	if (!GEngine) { return; }

	/*
	GetWorld()->GetTimerManager().SetTimer(DodgeTimerHandle,
										   this,
										   &UGladiatorMovementComponent::PrintDodgeDebug,
										   3.0f,
										   false);
	*/
}

float UGladiatorMovementComponent::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();

	if (bWantsToDodge)
	{
		MaxSpeed = MaxDodgeSpeed;
	}

	return MaxSpeed;
}

float UGladiatorMovementComponent::GetMaxAcceleration() const
{
	float MaxAccel = Super::GetMaxAcceleration();

	if (bWantsToDodge)
	{
		//float movementSimilarity = FVector::DotProduct(Velocity.GetSafeNormal(),
		//											   DodgeDirection.GetSafeNormal());

		//FMath::Clamp(movementSimilarity,
		//			 0.0f,
		//			 1.0f);

		//movementSimilarity = FMath::GetMappedRangeValueClamped(FVector2D(1.0f, 0.0f),
		//													   FVector2D(DodgeForwardAccelScale,
		//																 DodgeBackwardAccelScale),
		//													   movementSimilarity);

		//GEngine->AddOnScreenDebugMessage(-1,
		//								 DodgeTime,
		//								 FColor::Green,
		//								 *(FString::SanitizeFloat(movementSimilarity)));
		
		//FVector FlooredVelocity = 

		//FMath::GetMappedRangeValueClamped()

		//OLD - Set Accel Directly
		MaxAccel = MaxDodgeAcceleration;// *movementSimilarity;

		//MaxAccel = FMath::Abs(GetDodgeAcceleration(DodgeTime, DodgeDistance, MaxDodgeSpeed));

		GEngine->AddOnScreenDebugMessage(-1,
										 0.0f,
										 FColor::Silver,
										 *(FString::SanitizeFloat(MaxAccel)));
	}

	return MaxAccel;
}

FVector UGladiatorMovementComponent::GetAirControl(float DeltaTime, float TickAirControl, const FVector& FallAcceleration)
{
	if (bWantsToDodge)
	{
		TickAirControl = DodgeAirControl;
	}
	return Super::GetAirControl(DeltaTime,
								TickAirControl,
								FallAcceleration);
}


//===_DODGE_LOGIC_================================================================

float UGladiatorMovementComponent::GetDodgeCompletionPercentage() const
{
	if (!GWorld->GetTimerManager().IsTimerActive(DodgeTimerHandle))
	{
		GEngine->AddOnScreenDebugMessage(-1,
										 2.0f,
										 FColor::Red,
										 TEXT("Percentage Check Not Possible"));

		return 0.0f;
	}

	return FMath::GetMappedRangeValueClamped(FVector2D(0.0f, DodgeTime),
											 FVector2D(0.0f, 1.0f),
											 GWorld->GetTimerManager().GetTimerElapsed(DodgeTimerHandle));
}

float UGladiatorMovementComponent::GetDodgeAcceleration(float Duration, float Distance, float Speed) const
{
	return 2.0f * (Distance - (Speed * Duration)) * (1.0f / FMath::Pow(Duration, 2.0f));
}

//	COME BACK HERE!!!!!! ROTATE VECTOR OVER TIME BY ROTATION RATE

void UGladiatorMovementComponent::RotateDodgeInputTowards(FVector DesiredMoveDirection, float DodgeRotationRate)
{
	//DodgeDirection = FVector::Rotate
}


void UGladiatorMovementComponent::SetCanDodge(bool bSetCanDodge)
{
	bCanDodge = bSetCanDodge;
}

void UGladiatorMovementComponent::SetCanDodgeTrue()
{
	SetCanDodge(true);

	if (!GEngine) { return; }
	GEngine->AddOnScreenDebugMessage(-1,
									 1.0f,
									 FColor::Cyan,
									 TEXT("+++Can Dodge+++"));
}

void UGladiatorMovementComponent::SetCanDodgeFalse()
{
	SetCanDodge(false);

	if (!GEngine) { return; }
	GEngine->AddOnScreenDebugMessage(-1,
									 1.0f,
									 FColor::Red,
									 TEXT("---Cannot Dodge---"));
}

void UGladiatorMovementComponent::SetDodge(bool bDodging)
{
	bWantsToDodge = bDodging;
}


void UGladiatorMovementComponent::DoDodge()
{
	//Don't allow dodging if we're already dodging or cannot dodge
	//NOTE: THIS IS WHERE TO RECORD BUFFERING FOR SUBSEQUENT DODGES
	if (bWantsToDodge || bCurrentlyDodging || !bCanDodge) { return; }

	//bWantsToDodge is a flag that is replicated via the Client Side Prediction framework
	//as such, it is not explicitly marked with UPROPERTY(Replicated)
	bWantsToDodge = true;

	//MoveDirection = CharacterOwner->GetLastMovementInputVector();

	//BeginDodge();
}

//This should be called when the replicated flag bWantsToDodge is tripped
void UGladiatorMovementComponent::BeginDodge()
{
	//signal flag for Dodging
	//SetDodge(true);
	bWantsToDodge = false;
	bCurrentlyDodging = true;

	SetCanDodgeFalse();//lock us out from being able to dodge again

	//Bring Gladiator's movement speed to a good starting speed so acceleration is consistent
	//Velocity = FVector(0.0f, 0.0f, Velocity.Z);
	//FVector NewVelocity = Velocity.GetSafeNormal() * InitialDodgeSpeed;
	//FVector NewVelocity = DodgeDirection.GetSafeNormal() * InitialDodgeSpeed;
	//NewVelocity.Z = 0.0f; //Velocity.Z;
	//Velocity = NewVelocity;


	if (!IsMovingOnGround() && FMath::Sign(Velocity.Z) < 0.0f)
	{
		Velocity.Z = 0.0f;
	}


	//store the input from the x and y axes so we can see how close they are to zero
	float XMovInput = MoveDirection.X; //CharacterOwner->GetLastMovementInputVector().X;
	float YMovInput = MoveDirection.Y; //CharacterOwner->GetLastMovementInputVector().Y;

	//we aren't moving, so default to wanting to go forward
	if (FMath::IsNearlyZero(XMovInput) && FMath::IsNearlyZero(YMovInput))
	{
		DodgeDirection = CharacterOwner->GetActorForwardVector();
	}
	//we have a direction we want to move in, use that
	else
	{
		DodgeDirection = MoveDirection.GetSafeNormal();
	}

	//set the timer for End Dodge so we can have some time to move on the ground with Dodge values
	//make sure we do it in Begin Dodge so it happens on every client
	GetWorld()->GetTimerManager().SetTimer(DodgeTimerHandle,
										   this,
										   &UGladiatorMovementComponent::EndDodge,
										   DodgeTime,
										   false);
}

//This should execute if bCurrentlyDodging is true
//Note that bCurrentlyDodging is not replicated, but it is set wehn replicated flag bWantsToDodge is set in BeginDodge
void UGladiatorMovementComponent::ExecGroundDodge()
{
	//if (PawnOwner->IsLocallyControlled()) {

	//	FVector VelCopy = Velocity;
	//	VelCopy.Z = 0.0f;

	//	//store the input from the x and y axes so we can see how close they are to zero
	//	float XMovInput = CharacterOwner->GetLastMovementInputVector().X;
	//	float YMovInput = CharacterOwner->GetLastMovementInputVector().Y;
	//	//DodgeDirection.rota

	//}

	//constantly add input in the initial direction of the dodge
	//this ensures that the character will continue to move even if the player does not supply input
	//AddInputVector(DodgeDirection);
	
	if (DodgeSpeedCurve)
	{
		FVector NewVelocity = DodgeDirection * DodgeSpeedCurve->GetFloatValue(GetDodgeCompletionPercentage());
		NewVelocity.Z = Velocity.Z;

		Velocity = NewVelocity;
	}

}

void UGladiatorMovementComponent::EndDodge()
{
	//set replicated flag to signal we're stopping dodging
	//SetDodge(false);
	bCurrentlyDodging = false;

	//clean the Dodge Direction to safeguard against getting a dirty direction value at some point in the future
	DodgeDirection = FVector::ZeroVector;

	//Immediately set gladiator's movement speed to as fast as they can move when walking
	FVector NewVelocity = Velocity.GetSafeNormal() * MaxWalkSpeed;
	NewVelocity.Z = Velocity.Z;
	//Velocity = NewVelocity;


	//WIPE OLD TIMER FOR DODGE SO WE CAN LATER CHECK IF WE'RE STILL DODGIN
	GetWorld()->GetTimerManager().ClearTimer(DodgeTimerHandle);

	//START TIMER FOR DODGE COOLDOWN, WHICH WILL ALLOW DODGING AFTER TIMER IS COMPLETE
	GetWorld()->GetTimerManager().SetTimer(DodgeCooldownTimerHandle,
										   this,
										   &UGladiatorMovementComponent::SetCanDodgeTrue,
										   DodgeCooldownTime,
										   false);
}

/*
UGladiatorMovementComponent* UGladiatorMovementComponent::GetCharacterMovement()
{
	return 
}
*/

void UGladiatorMovementComponent::CleanupDodgeWhenDestroyed()
{
	//Clean up Timer Handles
	GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
}


//============================================================================
//REPLICATION

/*
void UGladiatorMovementComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	DOREPLIFETIME(UGladiatorMovementComponent, MoveDirection);
}
*/

void UGladiatorMovementComponent::UpdateFromCompressedFlags(uint8 Flags)
{
	Super::UpdateFromCompressedFlags(Flags);

	bWantsToDodge = (Flags&FSavedMove_Character::FLAG_Custom_0) != 0;
}


bool UGladiatorMovementComponent::Server_SetMoveDirection_Validate(const FVector& MoveDir)
{
	return true;
}

void UGladiatorMovementComponent::Server_SetMoveDirection_Implementation(const FVector& MoveDir)
{
	MoveDirection = MoveDir;
}

void UGladiatorMovementComponent::OnMovementUpdated(float DeltaSeconds,
													const FVector& OldLocation,
													const FVector& OldVelocity)
{
	Super::OnMovementUpdated(DeltaSeconds, OldLocation, OldVelocity);

	if (!CharacterOwner) { return; }

	//Store movement vector
	if (PawnOwner->IsLocallyControlled())
	{
		MoveDirection = PawnOwner->GetLastMovementInputVector();
		if (GetNetMode() == ENetMode::NM_Client)
		{
			Server_SetMoveDirection(MoveDirection);
		}
	}
	////Send Movement vector to Server
	//if (PawnOwner->Role < ROLE_Authority)
	//{
	//	Server_SetMoveDirection(MoveDirection);
	//}

	if (bWantsToDodge)
	{
		BeginDodge();
	}
	if (bCurrentlyDodging)
	{
		ExecGroundDodge();
	}
		
}

//PREDICTION HANDLING

class FNetworkPredictionData_Client* UGladiatorMovementComponent::GetPredictionData_Client() const
{
	check(PawnOwner != NULL);
	//THIS FAILS ON LISTEN SERVERS AND CAUSES CRASHES. IT WOULD BE REALLY NICE TO HAVE
	//check(PawnOwner->Role < ROLE_Authority);

	if (!ClientPredictionData)
	{
		UGladiatorMovementComponent* MutableThis = const_cast<UGladiatorMovementComponent*>(this);

		MutableThis->ClientPredictionData = new FNetworkPredictionData_Client_Gladiator(*this);
		MutableThis->ClientPredictionData->MaxSmoothNetUpdateDist = 92.0f;
		MutableThis->ClientPredictionData->NoSmoothNetUpdateDist = 140.0f;
	}

	return ClientPredictionData;
}

void FSavedMove_Gladiator::Clear()
{
	Super::Clear();

	//Clear variables back to their default values
	SavedMoveDirection = FVector::ZeroVector;
	bSavedWantsToDodge = false;
}

uint8 FSavedMove_Gladiator::GetCompressedFlags() const
{
	uint8 Result = Super::GetCompressedFlags();

	if (bSavedWantsToDodge)
	{
		Result |= FLAG_Custom_0;
	}

	return Result;
}

//Optimization for if the engine can combine moves
bool FSavedMove_Gladiator::CanCombineWith(const FSavedMovePtr& NewMove,
										  ACharacter* Character,
										  float MaxDelta) const
{
	if (SavedMoveDirection != ((FSavedMove_Gladiator*)&NewMove)->SavedMoveDirection)
	{
		return false;
	}

	return Super::CanCombineWith(NewMove, Character, MaxDelta);
}

void FSavedMove_Gladiator::SetMoveFor(ACharacter* Character,
									  float InDeltaTime,
									  FVector const& NewAccel,
									  class FNetworkPredictionData_Client_Character& ClientData)
{
	Super::SetMoveFor(Character,
					  InDeltaTime,
					  NewAccel,
					  ClientData);

	UGladiatorMovementComponent* CharMov = Cast<UGladiatorMovementComponent>(Character->GetCharacterMovement());
	if (CharMov)
	{
		SavedMoveDirection = CharMov->MoveDirection;
		bSavedWantsToDodge = CharMov->bWantsToDodge;
	}
}

void FSavedMove_Gladiator::PrepMoveFor(class ACharacter* Character)
{
	Super::PrepMoveFor(Character);

	UGladiatorMovementComponent* CharMov = Cast<UGladiatorMovementComponent>(Character->GetCharacterMovement());
	if (CharMov)
	{
		CharMov->MoveDirection = SavedMoveDirection;
		CharMov->bWantsToDodge = bSavedWantsToDodge;
	}
}

FNetworkPredictionData_Client_Gladiator::FNetworkPredictionData_Client_Gladiator(const UCharacterMovementComponent& ClientMovement)
	: Super(ClientMovement)
{

}

FSavedMovePtr FNetworkPredictionData_Client_Gladiator::AllocateNewMove()
{
	return FSavedMovePtr(new FSavedMove_Gladiator());
}
