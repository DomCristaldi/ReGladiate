// Fill out your copyright notice in the Description page of Project Settings.

#include "ReGladiate.h"
#include "Gladiator.h"

#include "GladiatorMovementComponent.h"

// Sets default values
AGladiator::AGladiator(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get())
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UGladiatorMovementComponent>(ACharacter::CharacterMovementComponentName))
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//ObjectInitializer = FObjectInitializer::Get();
}

// Called when the game starts or when spawned
void AGladiator::BeginPlay()
{
	Super::BeginPlay();
	
	//GetCharacterMovement()->SetMovementMode(MOVE_Custom, EGladiatorMovementMode::Boosting);
}

// Called every frame
void AGladiator::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void AGladiator::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	//Super::SetupPlayerInputComponent(InputComponent);
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	//PlayerInputComponent->BindAction("Dodge", IEPressed, )

	PlayerInputComponent->BindAxis("MoveForward", this, &AGladiator::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AGladiator::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
}

void AGladiator::AddMovementInput(FVector WorldDirection, float ScaleValue, bool bForce)
{

	Super::AddMovementInput(WorldDirection, ScaleValue, bForce);
	return;

	UGladiatorMovementComponent* MoveComp = Cast<UGladiatorMovementComponent>(GetMovementComponent());

	if (MoveComp && MoveComp->bWantsToDodge)
	{
		Super::AddMovementInput(FVector::ZeroVector, ScaleValue, bForce);
	}
	else
	{
		Super::AddMovementInput(WorldDirection, ScaleValue, bForce);
	}

}

/*
UPawnMovementComponent* AGladiator::GetMovementComponent()
{
	return 
}
*/
void AGladiator::MoveForward(float Val)
{

	//GetMovementComponent();
	UGladiatorMovementComponent* MoveComp = Cast<UGladiatorMovementComponent>(GetMovementComponent());
	if (Val != 0.0f)
	{
		//if (MoveComp && MoveComp->bWantsToDodge) {

		//}
		//else
		//{
			AddMovementInput(GetActorForwardVector(), Val);
		//}
	}
}

void AGladiator::MoveRight(float Val)
{
	UGladiatorMovementComponent* MoveComp = Cast<UGladiatorMovementComponent>(GetMovementComponent());

	if (Val != 0.0f)
	{
		//if (MoveComp && MoveComp->bWantsToDodge)
		//{

		//}
		//else
		//{
			AddMovementInput(GetActorRightVector(), Val);
		//}
	}
}

