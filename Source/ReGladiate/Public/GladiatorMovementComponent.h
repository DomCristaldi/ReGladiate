// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/CharacterMovementComponent.h"
#include "GladiatorMovementComponent.generated.h"

/**
 * 
 */

//ENUM THAT DEFINES OUR CUSTOM MOVEMENT MODES
UENUM(BlueprintType)
enum class EGladiatorMovementMode : uint8
{
	NONE,
	Walking,
	Boosting
};

UCLASS()
class REGLADIATE_API UGladiatorMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:

	virtual float GetMaxSpeed() const override;
	virtual float GetMaxAcceleration() const override;

	virtual FVector GetAirControl(float DeltaTime, float TickAirControl, const FVector& FallAcceleration) override;
	
protected:
	FTimerHandle DodgeTimerHandle;
	FTimerHandle DodgeCooldownTimerHandle;

	//float SavedMaxWalkSpeed;
	//float SavedBrakigDecelerationWalking;

public:

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Movement Mode")
	EGladiatorMovementMode currentGladiatorMoveMode;

	//UPROPERTY(EditAnywhere, Category = "Gladiator Movement: Dodge")
	//float DodgeStrength;

	//UPROPERTY(EditAnywhere, Category = "Gladiator Movement: Dodge")
	//float GroundDodgeStrengthMultiplier;

	//UPROPERTY(EditAnywhere, Category = "Gladiator Movement: Dodge")
	float DodgeDistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gladiator Movement: Dodge")
	UCurveFloat* DodgeSpeedCurve;

	UPROPERTY(EditAnywhere, Category = "Gladiator Movement: Dodge")
	float MaxDodgeSpeed;

	UPROPERTY(EditAnywhere, Category = "Gladiator Movement: Dodge")
	float InitialDodgeSpeed;

	UPROPERTY(EditAnywhere, Category = "Gladiator Movement: Dodge")
	float MaxDodgeAcceleration;

	//UPROPERTY(EditAnywhere, Category = "Gladiator Movement: Dodge")
	float DodgeForwardAccelScale;

	//UPROPERTY(EditAnywhere, Category = "Gladiator Movement: Dodge")
	float DodgeBackwardAccelScale;

	UPROPERTY(EditAnywhere, Category = "Gladiator Movement: Dodge",
			  meta = (ClampMin = "0.0", ClampMax = "1.0",
					  UIMin = "0.0",	UIMax = "1.0"))
	float DodgeAirControl;

	UPROPERTY(EditAnywhere, Category = "Gladiator Movement: Dodge")
	float DodgeTime;
	
	UPROPERTY(EditAnywhere, Category = "Gladiator Movement: Dodge")
	float DodgeCooldownTime;

	UPROPERTY(EditAnywhere, Category = "Gladiator Movement: Dodge")
	uint8 bCanDodge : 1;

	uint8 bWantsToDodge : 1;
	uint8 bCurrentlyDodging : 1;

	UPROPERTY(/*Replicated*/)
	FVector MoveDirection;

protected:
	FVector DodgeDirection;

public:
	UFUNCTION(BlueprintPure, Category = "Gladiator Movement: Dodge")
	float GetDodgeCompletionPercentage() const;

	UFUNCTION()
	float GetDodgeAcceleration(float Duration, float Distance, float Speed) const;

	UFUNCTION(BlueprintCallable, Category = "Gladiator Movement: Dodge")
	void RotateDodgeInputTowards(FVector DesiredMoveDirection, float DodgeRotationRate);

protected:

	//constructor
	UGladiatorMovementComponent(const class FObjectInitializer& ObjectInitializer);

	//INIT
	virtual void InitializeComponent() override;

	//DESTROY
	virtual void DestroyComponent(bool bPromoteChildren) override;

	//FIRST FRAME
	virtual void BeginPlay() override;

	//UPDATE
	virtual void TickComponent(float DeltaTime,
							   enum ELevelTick TickType,
							   FActorComponentTickFunction* ThisTickFunction) override;

	//Handling of our custom movement logic happens in here
	virtual void PhysCustom(float deltaTime, int32 Iterations) override;

	//our custom boost movement logic
	void PhysBoostMovement(float deltaTime, int32 Iterations);
	

	UFUNCTION(Reliable, Server, WithValidation)
	void Server_SetMoveDirection(const FVector& MoveDir);
	

	virtual void OnMovementUpdated(float DeltaSeconds,
								   const FVector& OldLocation,
								   const FVector& OldVelocity) override;

public:
	virtual void SetCanDodge(bool bSetCanDodge);
	//needed so we can call SetCanDodge from a timer manager
	void SetCanDodgeTrue();
	void SetCanDodgeFalse();

	virtual void SetDodge(bool bDodging);

	UFUNCTION(BlueprintCallable, Category = "Gladiator: Dodge")
	void DoDodge();

	void BeginDodge();

	void ExecGroundDodge();

	void EndDodge();

	UFUNCTION()
	void CleanupDodgeWhenDestroyed();


//==================================================================================
//REPLICATION

public:

	//UGladiatorMovementComponent* GetCharacterMovement();

	virtual void UpdateFromCompressedFlags(uint8 Flags) override;

	virtual class FNetworkPredictionData_Client* GetPredictionData_Client() const override;

};


class FSavedMove_Gladiator : public FSavedMove_Character
{
public:
	
	typedef FSavedMove_Character Super;

	FVector SavedMoveDirection;
	uint8 bSavedWantsToDodge : 1;

	virtual void Clear() override;

	virtual uint8 GetCompressedFlags() const override;

	virtual bool CanCombineWith(const FSavedMovePtr& NewMove,
								ACharacter* Character,
								float MaxDelta) const override;

	virtual void SetMoveFor(ACharacter* Character,
							float InDeltaTime,
							FVector const& NewAccel,
							class FNetworkPredictionData_Client_Character& ClientData) override;

	///@brief This is used to copy state from the saved move to the character movement component.
	///This is ONLY used for predictive corrections, the actual data must be sent through RPC.
	virtual void PrepMoveFor(class ACharacter* Character) override;
};


class FNetworkPredictionData_Client_Gladiator : public FNetworkPredictionData_Client_Character
{
public:

	FNetworkPredictionData_Client_Gladiator(const UCharacterMovementComponent& ClientMovement);

	typedef FNetworkPredictionData_Client_Character Super;

	virtual FSavedMovePtr AllocateNewMove() override;
};